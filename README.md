Q1)For the first question, a structure similar to the Selection Sort algorithm was used. 4 outputs have been prepared for testing the algorithm.
default input1 :{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
default output1:{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "kf", "zc", "ef", "l"}

input2:{"aaaasd", "a", "aab", "aaabcd", "cssssssd", "fdz", "zc", "lklklklklklklklkl", "l", "aaaaaft"}
output2:{"aaaaaft", "aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "zc", "l"}

input3:{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "lklklklklklklklkl", "l", "aaaaaft", "aak"}
output3:{"aaaaaft", "aaaasd", "aaabcd", "aab", "aak", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "l"}

input4:{"aaaasd", "a", "aab", "aaabcd", "cssssssd", "fdz", "kf", "lklklklklklklklkl", "l", "aaaaaft", "aak", "bvkbvkzbvözbvkv"}
output4:{"aaaaaft", "aaaasd", "aaabcd", "aab", "aak", "a", "lklklklklklklklkl", "bvkbvkzbvözbvkv", "cssssssd", "fdz", "kf", "l"}

Q2)The question created for the given input and output is "Write a function that starts from the value that comes as a parameter and divides by 2 and continues until one.It must return integers greater than one in ascending order (fractional numbers should be rounded down)"
input1 :9
output1 :2,4,9

input2 :23
output2 :23 11 5 2

Q3)As in the first question, the most repeated word was found with a structure similar to the Selection Sort algorithm and 4 test data were prepared.
default input1 :{"apple", "pie", "apple", "red", "red", "red"}
default output1:red

input2:{"apple", "pie", "apple", "red", "red", "red", "pie", "pie", "pie"}
output2:pie

input3:{"apple", "pie", "apple", "red", "red", "red", "pie", "pie", "pie", "red"}
output3:pie

input4:{"apple", "pie", "apple", "apple", "red", "apple", "red", "apple", "red", "pie", "apple", "pie", "pie", "red", "apple"}
output4:apple
         
