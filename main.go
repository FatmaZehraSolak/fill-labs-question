package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println("Question 1 Test Result")
	question1Test()
	fmt.Println("Question 2 Default Test Result")
	question2(9)
	fmt.Println("Question 3 Test Result")

	question3Test()
}


func question1(subsequentArray []string) []string {
	var sequentArrays = append(subsequentArray)
	var temp string

	for i := 0; i < len(subsequentArray); i++ {
		temp = subsequentArray[i]
		//The element in each index is assigned a temporary value and compared to all indexes after it.
		for j := i + 1; j < len(subsequentArray); j++ {
			//if a word containing more letters a is found in index j than an element in index i, the element in index j is assigned a temporary value and the element in index i is replaced with an element in index j
			if strings.Count(temp, "a") < strings.Count(subsequentArray[j], "a") {
				temp = subsequentArray[j]
				subsequentArray[j] = subsequentArray[i]
				subsequentArray[i] = temp
			  //If the element at index j and index i contains an equal number of letters a, the longer element is assigned to a temporary variable and the element at index i is replaced with an item at index j.
			} else if strings.Count(temp, "a") == strings.Count(subsequentArray[j], "a") {
				if len(subsequentArray[j]) > len(temp) {
					temp = subsequentArray[j]
					subsequentArray[j] = subsequentArray[i]
					subsequentArray[i] = temp
				}

			}

		}
		sequentArrays[i] = temp

	}
	fmt.Println(sequentArrays)

	return sequentArrays
}
func question1Test() {
	if Equal([]string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "kf", "zc", "ef", "l"}, question1([]string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"})) == true {
		fmt.Println("default question successful")
	} else {
		fmt.Println("default question failed")
	}
	if Equal([]string{"aaaaaft", "aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "zc", "l"}, question1([]string{"aaaasd", "a", "aab", "aaabcd", "cssssssd", "fdz", "zc", "lklklklklklklklkl", "l", "aaaaaft"})) == true {
		fmt.Println("test 2 successful")
	} else {
		fmt.Println("test 2 failed")
	}
	if Equal([]string{"aaaaaft", "aaaasd", "aaabcd", "aab", "aak", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "l"}, question1([]string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "lklklklklklklklkl", "l", "aaaaaft", "aak"})) == true {
		fmt.Println("test 3 successful")
	} else {
		fmt.Println("test 3 failed")
	}
	if Equal([]string{"aaaaaft", "aaaasd", "aaabcd", "aab", "aak", "a", "lklklklklklklklkl", "bvkbvkzbvözbvkv", "cssssssd", "fdz", "kf", "l"}, question1([]string{"aaaasd", "a", "aab", "aaabcd", "cssssssd", "fdz", "kf", "lklklklklklklklkl", "l", "aaaaaft", "aak", "bvkbvkzbvözbvkv"})) == true {
		fmt.Println("test 4 successful")
	} else {
		fmt.Println("test 4 failed")
	}
}
//function that checks if arrays are equal
func Equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
func question2(a int)  {
	/*Write a function that starts from the value that comes as a parameter and divides by 2 and continues until one.
	It must return integers greater than one in ascending order (fractional numbers should be rounded down) */

	if a <= 1 {
		return
	} else {
		question2(a / 2)
		if a > 1 {
			fmt.Println(a)
		}

	}
}
func question3(parameterArray []string) string {
	var mostRepeatedData string
	var mostRepeatedDataCount int
	var tempData string
	var tempCount int
	mostRepeatedData = parameterArray[0]
	mostRepeatedDataCount = 0
	//repeat count is calculated by traversing the array for all elements in order
	for i := 0; i < len(parameterArray); i++ {
		tempData = parameterArray[i]
		tempCount = 0
		for j := i + 1; j < len(parameterArray); j++ {
			if parameterArray[i] == parameterArray[j] {
				tempCount++
			}

		}
		if tempCount > mostRepeatedDataCount {
			mostRepeatedDataCount = tempCount
			mostRepeatedData = tempData
		}

	}

	fmt.Println(mostRepeatedData)
	return mostRepeatedData
}

func question3Test() {

	if question3([]string{"apple", "pie", "apple", "red", "red", "red"}) == "red" {
		fmt.Println("default question successful")
	} else {
		fmt.Println("default question failed")
	}

	if question3([]string{"apple", "pie", "apple", "red", "red", "red", "pie", "pie", "pie"}) == "pie" {
		fmt.Println("test 2 successful")
	} else {
		fmt.Println("test 2 failed")
	}
	if question3([]string{"apple", "pie", "apple", "red", "red", "red", "pie", "pie", "pie", "red"}) == "pie" {
		fmt.Println("test 3 successful")
	} else {
		fmt.Println("test 3 failed")
	}
	if question3([]string{"apple", "pie", "apple", "apple", "red", "apple", "red", "apple", "red", "pie", "apple", "pie", "pie", "red", "apple"}) == "apple" {
		fmt.Println("test 4 successful")
	} else {
		fmt.Println("test 4 failed")
	}

}
