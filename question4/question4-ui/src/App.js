import MainComponent from './components/main'
import React  from 'react';
function App() {
  
  return (
    <div className="App">
    
     <MainComponent/>
    </div>
  );
}

export default App;
