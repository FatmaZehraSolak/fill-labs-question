import React, { useState, useEffect, useRef } from "react";
import { AgGridColumn, AgGridReact } from "ag-grid-react";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Button,
} from "@material-ui/core";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>;
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export default function MainComponent() {
  const [rows, setrows] = useState([]);
  const [selectedRowId, setselectedRowId] = useState();
  const [selectedRowName, setselectedRowName] = useState();
  const [selectedRowSurname, setselectedRowSurname] = useState();
  const [AlertOpen, setAlertOpen] = useState(false);
  const [dialogPage, setdialogPage] = useState(false);
  const [dialogPageType, setdialogPageType] = useState();
  const [newUserName, setnewUserName] = useState("");
  const [newUserSurname, setnewUserSurname] = useState("");
  const gridRef = useRef(null);
  useEffect(() => {
    //Refreshes the data grid structure to list all users on page refresh
    try {
      fetch("http://localhost:8000/getall", {
        method: "GET",
        headers: { "Content-Type": "application/json" },
      })
        .then((response) => response.json())
        .then(async (json) => {
          var dene = json;
          setrows([]);

          dene.forEach((element) => {
            setrows((rows) => [
              ...rows,
              {
                id: element.ID,
                userName: element.Name,
                userSurname: element.Surname,
              },
            ]);
          });
        });
    } catch (error) {
      console.log(error);
    }
  }, []);
//Opens the dialog screen by setting the existing variables for the dialog to be opened according to their types when the buttons are clicked.
  function onClickButtons(type) {

    if (type === "newUser") {
      setdialogPageType("New User");
      setdialogPage(true);
    } else {
     // If the edit or delete button is clicked, the information of the user to be changed or deleted is transferred to the variables from the data grid structure.
      const selectedNode = gridRef.current.api.getSelectedNodes();
      if (selectedNode.length === 0) {
        setAlertOpen(true);
        setTimeout(function () {
          setAlertOpen(false);
        }, 2000);
      } else {
        setselectedRowId(selectedNode[0].data.id);
        setselectedRowName(selectedNode[0].data.userName);
        setselectedRowSurname(selectedNode[0].data.userSurname);
        setdialogPageType(type);
        setdialogPage(true);
      }
    }
  }

  function backButtonClickdialogPage() {
    setdialogPage(false);
  }

  function actionButtonClick(type) {
    if (type === "Edit") {
      /*When the edit button is clicked, the new data entered by the user is sent to the server for updating in the database and
       the server returns the updated user information. Data grid structure is updated with incoming new data. */
      try {
        fetch("http://localhost:8000/update", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            Surname: selectedRowSurname,
            Name: selectedRowName,
            Id: selectedRowId,
          }),
        })
          .then((response) => response.json())
          .then(async (json) => {
            setrows([]);
            //If there is more than one element, they are added to the datagrid structure one by one.
            if (json.length !== undefined) {
              json.forEach((element) => {
                setrows((rows) => [
                  ...rows,
                  {
                    id: element.ID,
                    userName: element.Name,
                    userSurname: element.Surname,
                  },
                ]);
              });
            } else {
              setrows((rows) => [
                ...rows,
                { id: json.ID, userName: json.Name, userSurname: json.Surname },
              ]);
            }
          });
      } catch (error) {
        console.log(error);
      }
      setdialogPage(false);
    } else if (type === "Delete") {
      /*The id of the user to be deleted is sent to the server and after the desired user is deleted, the server sends a list of new users.
       Data grid structure is updated with incoming new data. */
      try {
        console.log(selectedRowId);
        fetch("http://localhost:8000/delete/" + selectedRowId, {
          method: "GET",
          headers: { "Content-Type": "application/json" },
        })
          .then((response) => response.json())
          .then(async (json) => {

            setrows([]);
            //If there is more than one element, they are added to the datagrid structure one by one.
            if (json.length !== undefined) {
              json.forEach((element) => {
                setrows((rows) => [
                  ...rows,
                  {
                    id: element.ID,
                    userName: element.Name,
                    userSurname: element.Surname,
                  },
                ]);
              });
            } else {
              setrows((rows) => [
                ...rows,
                { id: json.ID, userName: json.Name, userSurname: json.Surname },
              ]);
            }
          });
      } catch (error) {
        console.log(error);
      }
      setdialogPage(false);
    } else {
      //new user data is sent to the server for inclusion in the database. After the server registers the user, it sends a new user list.
      //Data grid structure is updated with incoming new data.
      try {
        fetch("http://localhost:8000/create", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            surname: newUserSurname,
            name: newUserName,
          }),
        })
          .then((response) => response.json())
          .then(async (json) => {
            setrows([]);

            if (json.length !== undefined) {
              json.forEach((element) => {
                setrows((rows) => [
                  ...rows,
                  {
                    id: element.ID,
                    userName: element.Name,
                    userSurname: element.Surname,
                  },
                ]);
              });
            } else {
              setrows((rows) => [
                ...rows,
                { id: json.ID, userName: json.Name, userSurname: json.Surname },
              ]);
            }
          });
      } catch (error) {
        console.log(error);
      }
      setdialogPage(false);
    }
  }

  return (
    <div className="ag-theme-alpine" style={{ height: 400, width: 800 }}>
      <Button color="primary" onClick={(e) => onClickButtons("newUser")}>
        New
      </Button>
      <Button color="primary" onClick={(e) => onClickButtons("Edit")}>
        Edit
      </Button>
      <Button color="primary" onClick={(e) => onClickButtons("Delete")}>
        Delete
      </Button>
      <AgGridReact ref={gridRef} rowSelection="single" rowData={rows}>
        <AgGridColumn
          field="id"
          sortable={true}
          filter={true}
          checkboxSelection={true}
        ></AgGridColumn>

        <AgGridColumn field="userName"></AgGridColumn>
        <AgGridColumn field="userSurname"></AgGridColumn>
      </AgGridReact>
      {AlertOpen && (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={AlertOpen}
          autoHideDuration={6000}
        >
          <Alert severity="error">You must select a user from the table</Alert>
        </Snackbar>
      )}
      {dialogPage && (
        <Dialog open={dialogPage} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">
            {dialogPageType} Dialog
          </DialogTitle>

          <DialogContent>
            {dialogPageType === "Delete" ? (
              "Are You Sure?"
            ) : dialogPageType === "Edit" ? (
              <div>
                <TextField
                  autoFocus
                  margin="dense"
                  label="Edit User Name"
                  fullWidth
                  value={selectedRowName}
                  onChange={(e) => setselectedRowName(e.target.value)}
                />
                <TextField
                  
                  margin="dense"
                  label="Edit User Surname"
                  fullWidth
                  value={selectedRowSurname}
                  onChange={(e) => setselectedRowSurname(e.target.value)}
                />
              </div>
            ) : (
              <div>
                <TextField
                  autoFocus
                  margin="dense"
                  label="İnput New User Name"
                  fullWidth
                  onChange={(e) => setnewUserName(e.target.value)}
                />
                <TextField
                  
                  margin="dense"
                  label="İnput New User Surname"
                  fullWidth
                  onChange={(e) => setnewUserSurname(e.target.value)}
                />
              </div>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={backButtonClickdialogPage} color="primary">
              Back
            </Button>
            <Button
              onClick={(e) => actionButtonClick(dialogPageType)}
              color="primary"
            >
              {dialogPageType === "Delete"
                ? "Delete"
                : dialogPageType === "Edit"
                ? "Save"
                : "Create"}
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </div>
  );
}
