Prerequisites
You'll need Go version [1.16+](https://golang.org/dl/) and [PostgreSQL](https://www.postgresql.org/download/) installed on your development machine.
 
If postegro has not been downloaded to your computer, a heroku postgre link has been added to be used in the project. Lines 47. and 48. in the main.go file are the part to be used for the heroku postegre connection. If you have postegre installed on your own computer, use line 53. in the main.go file to connect to your database.

Getting started
you'll need to install
go get github.com/jinzhu/gorm
go get github.com/gorilla/mux
go get github.com/lib/pq
go get github.com/rs/cors

Gorm:
The GORM is fantastic ORM library for Golang, aims to be developer friendly. It is an ORM library for dealing with relational databases. This gorm library is developed on the top of database/sql package.

To install GORM just use the following command :
go get github.com/jinzhu/gorm

Mux:
Package gorilla/mux implements a request router and dispatcher for matching incoming requests to their respective handler.

The name mux stands for "HTTP request multiplexer". Like the standard http.ServeMux, mux.Router matches incoming requests against a list of registered routes and calls a handler for the route that matches the URL or other conditions.
[more information](https://github.com/gorilla/mux)

To install Mux just use the following command :
go get github.com/gorilla/mux

A pure Go postgres driver for Go's database/sql package
To install Mux just use the following command :
go get github.com/lib/pq

go get github.com/rs/cors
