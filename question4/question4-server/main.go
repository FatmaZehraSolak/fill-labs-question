package main

import (
	"encoding/json"

	"log"
	"net/http"
    "fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"github.com/rs/cors"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)
//A struct structure is created for the table to be created
type Users struct {
	gorm.Model
	
	Name      string
	Surname   string

}
//A struct suitable for the json structure in the body of the edit request is created to be used later when decoding the json
type EditBody struct {
    
	Id        int	`json:"Id"`
	Name      string `json:"Name"`
	Surname      string `json:"Surname"`


}
var db *gorm.DB
var err error
//By default, we define the data to be added to the database (it is done so that the newly created table structure is not empty)
var (
	users = []Users{
		{Name: "Ahmet Hamdi",Surname:"Tampınar"},
		{Name: "Yahya Kemal",Surname:"Beyatlı"},
		{Name: "Orhan Veli",Surname:" Kanık"},
	}
	
)

func main() {
	//A request router is defined to match incoming requests with their respective handlers
	router := mux.NewRouter()
	//If you don't have postegro installed on your computer, connect to postegro with the postegro database connection created in heroku.
	dbUri := fmt.Sprintf("postgres://vfnrdbbpnhqcdo:62770bc84424471c30f5a1616c9483e544ca079d99eb61aa582a04809b471e78@ec2-54-220-195-236.eu-west-1.compute.amazonaws.com:5432/da3u7iqscugvue") 
	db, err =gorm.Open("postgres", dbUri)

	
   //If postegro is installed on your computer, enter your own information here To connect to the postegre database, enter the user name, user password, host, database name parameters.
  // db, err = gorm.Open("postgres", "host=localhost port=5433 user=postgres dbname=postgres sslmode=disable password=2382985")

	if err != nil {
		panic("failed to connect database")
	}
    //closing the database when it is not in use 
	defer db.Close()

    // automatically create the table based on your model
	db.AutoMigrate(&Users{})

    //Loading default data into created table
	for index := range users {
		db.Create(&users[index])
	}


    //We direct the incoming http requests to the relevant functions with the router we defined at the beginning of the application.
	router.HandleFunc("/getall", GetAll).Methods("GET")
	router.HandleFunc("/get/{id}", GetUser).Methods("GET")
	router.HandleFunc("/delete/{id}", DeleteUser).Methods("GET")
	router.HandleFunc("/create",create).Methods("POST")
	router.HandleFunc("/update",UpdateUser).Methods("POST")
	handler := cors.Default().Handler(router)
    //We activate our server with the router we created on the 8000th port.
	log.Fatal(http.ListenAndServe("0.0.0.0:8000", handler))
}

func create(response http.ResponseWriter, r *http.Request) {
	var user Users
    //We decode the body of the http request and assign it to a variable
    err := json.NewDecoder(r.Body).Decode(&user)

    if err != nil {
        http.Error(response, err.Error(), http.StatusBadRequest)
        return
    }
	//If decoded without error, new data is added to the database.
	db.Create(&user)
	//All data suitable for the user struct structure are found in the database and encoded into the json structure and sent in response to the incoming request.
	var users []Users
	db.Find(&users)
	json.NewEncoder(response).Encode(&users)
}
func UpdateUser(response http.ResponseWriter, request *http.Request) {

	var user EditBody
    //We decode the body of the http request and assign it to a variable
    err := json.NewDecoder(request.Body).Decode(&user)

    if err != nil {
        http.Error(response, err.Error(), http.StatusBadRequest)
        return
    }
	////If the code is decoded without error, the relevant places in the database of the user we give the id in the table we want are updated.
	db.Table("users").Where("id = ?", user.Id).Update("name", user.Name,"surname",user.Surname)
	db.Table("users").Where("id = ?", user.Id).Update("surname",user.Surname)
	//All data suitable for the user struct structure are found in the database and encoded into the json structure and sent in response to the incoming request.
	var users []Users
	db.Find(&users)
	json.NewEncoder(response).Encode(&users)
	

}
func GetAll(response http.ResponseWriter, request *http.Request) {
	//All data suitable for the user struct structure are found in the database and encoded into the json structure and sent in response to the incoming request.
	fmt.Println("send all")
	var users []Users
	db.Find(&users)
	json.NewEncoder(response).Encode(&users)
}

func GetUser(response http.ResponseWriter, request *http.Request) {
	//Using mux library, incoming parameters in http request are decoded
	params := mux.Vars(request)
	var user Users
	//Finds its id from the user database we specifically specified.
	db.First(&user, params["id"])
	//encoded into the json structure and sent in response to the incoming request
	json.NewEncoder(response).Encode(&user)
}


func DeleteUser(response http.ResponseWriter, request *http.Request) {
	//Using mux library, incoming parameters in http request are decoded
	params := mux.Vars(request)
	var user Users
	//Finds its id from the user database we specifically specified.
	db.First(&user, params["id"])
	//that data is deleted from the database
	db.Delete(&user)
	

   //All data suitable for the user struct structure are found in the database and encoded into the json structure and sent in response to the incoming request.
	var users []Users
	db.Find(&users)
	json.NewEncoder(response).Encode(&users)
}
